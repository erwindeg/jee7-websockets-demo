package nl.sogeti.websockets;

import javax.websocket.OnMessage;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/echo")
public class EchoBean {

	@OnMessage
	public String echo(String message) {
		return message + " (from your server)";
	}

}
