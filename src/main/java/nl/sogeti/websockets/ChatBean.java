package nl.sogeti.websockets;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@Singleton
@ServerEndpoint("/chatsocket")
public class ChatBean {

	private Set<Session> clients = new HashSet<>();

	@OnOpen
	public void onSocketOpened(Session session) throws IOException {
		clients.add(session);
	}

	@OnClose
	public void onSocketClosed(Session session) throws IOException {
		clients.remove(session);
	}

	@OnMessage
	public void onReceivedFrame(Session session,String message) throws IOException {
		for(Session client : clients){
			client.getAsyncRemote().sendText(session.getId()+": "+message);
		}
	}

}
